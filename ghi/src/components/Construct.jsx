import React, { useState } from 'react';
import CreateTripForm from './Pages/CreateTripForm';
import TripList from './Pages/TripList';

//@ts-check
/**
 * @typedef {{module: number, week: number, day: number, min: number, hour: number}} LaunchInfo
 *
 * @param {{info: LaunchInfo | undefined }} props
 * @returns {React.ReactNode}
 */
function Construct(props) {
    const [showCreateTripForm, setShowCreateTripForm] = useState(false);
    const [tripUpdated, setTripUpdated] = useState(false);


    const handleTripCreated = () => {
        setTripUpdated(!tripUpdated);
    };

    if (!props.info) {
        return <p>Loading...</p>
    }

    return (
        <React.Fragment>
            <div className="px-2">
                <h1>Under construction</h1>
                <h2>Coming on (or before)</h2>
                <h2>
                    Module: {props.info.module} Week: {props.info.week} Day:{' '}
                    {props.info.day}
                </h2>
                <h2>
                    by or <strong>WELL BEFORE</strong> {props.info.hour}:
                    {props.info.min} Cohort Time
                </h2>
                <button onClick={() => setShowCreateTripForm(!showCreateTripForm)}>
                    {showCreateTripForm ? 'Hide Create Trip Form' : 'Show Create Trip Form'}
                </button>
                {showCreateTripForm && <CreateTripForm onTripCreated={handleTripCreated} />}
                <TripList tripUpdated={tripUpdated} />
            </div>
        </React.Fragment>
    )
}

export default Construct
