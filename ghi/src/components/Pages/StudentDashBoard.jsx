import '../../App/JoshuaStyles.css'
import { NavLink } from 'react-router-dom'
import { useState, useEffect } from 'react';
import React from 'react';
import user from '../../hooks/useAuthService';


export default function StudentDashBoardForm() {

    const [getPoints, setPoints] = useState([]);
    const [getUser, setUser] = useState([]);

    const pointGet = async () => {
        const user_data = await fetch('http://localhost:8000/api/auth/authenticate', {
            credentials: 'include'
        })
        let data;
        if (user_data.ok) {
            data = await user_data.json();
            setUser(data);
        }
        const response = await fetch(`http://localhost:8000/users/${data.user_id}`);
        if (response.ok) {
            const pointData = await response.json();
            setPoints(pointData);
        }
        else {
            console.error('Error');
        }
    }

    useEffect(() => {
        pointGet();
    }, []);

    return (
        <div className="page-wrapper2 p-2">
            <h1 className="pt-2" style={{ textAlign: 'center', fontSize: '32px' }}>
                Dashboard
            </h1>
            <div className="events-wrapper2" style={{ paddingLeft: '64px', paddingRight: '64px', paddingTop: '16px', paddingBottom: '24px' }}>
                <div className="events-actions">
                    <div className="card trip-2 p-3">
                        <div className="trip-container pt-2">
                            <div className="trip-items">
                                <div className="py-3" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%', height: '10%', transform: 'translate(0px,-13px)' }}>
                                    <div style={{ fontSize: '24px' }}>
                                        Vote On Our Next Trips!
                                    </div>
                                </div>
                                <div className="trip-listings">
                                    <div className="center-box" style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                                        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%' }}>
                                            Trip 1 (Placeholder)
                                        </div>
                                    </div>
                                    <div className="center-box" style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                                        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%' }}>
                                            Trip 2 (Placeholder)
                                        </div>
                                    </div>
                                    <div className="center-box" style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                                        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%' }}>
                                            Trip 3 (Placeholder)
                                        </div>
                                    </div>
                                    <div className="py-1" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%', height: '10%', transform: 'translate(0px,13px)' }}>
                                        <div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="py-3" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                        </div>
                        <div className="gallery-3 ps-1">
                            <div className="gallery-container" style={{ alignItems: 'center' }}>
                                <div className="bottom-bar1" style={{ height: '90%', width: '25%' }}>
                                    <button className="custom-button">
                                        <NavLink to="http://localhost:5173/trips">List of Trips</NavLink>
                                    </button>
                                </div>
                                <div className="divider bottom-bar1" style={{ height: '90%', width: '25%' }}>
                                    <h2>Points</h2>
                                    {
                                        getPoints ?
                                            <h3>{getPoints.points}</h3>
                                            :
                                            <h3>0</h3>
                                    }
                                </div>
                                <div className="bottom-bar1" style={{ height: '90%', width: '25%' }}>
                                    <button className="custom-button">
                                        <NavLink to="http://localhost:5173/create-trip">Student Trip Form</NavLink>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}
