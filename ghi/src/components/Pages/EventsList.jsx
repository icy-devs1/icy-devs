import React, { useEffect, useState } from 'react';
import CreateEventForm from './CreateEvent';
import EditEventForm from './EditEvent';

function EventsList() {
    const [eventsList, setEvents] = useState([]);
    const [editingEvent, setEditingEvent] = useState(null);

    const fetchData = async () => {
        try {
            const url = 'http://localhost:8000/events/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setEvents(data);
            } else {
                throw new Error('Failed to fetch Events');
            }
        } catch (error) {
            console.error('Error fetching Events:', error);
        }
    };

    const handleEventCreated = (newEvent) => {
        setEvents((prevEvents) => [...prevEvents, newEvent]);
    };

    const handleDelete = async (eventId) => {
        try {
            const url = `http://localhost:8000/events/${eventId}`;
            const response = await fetch(url, { method: 'DELETE' });
            if (response.ok) {
                setEvents(eventsList.filter(event => event.event_id !== eventId));
            } else {
                throw new Error('Failed to delete event');
            }
        } catch (error) {
            console.error('Error deleting event:', error);
        }
    };

    const handleEdit = (event) => {
        setEditingEvent(event);
    };

    const handleEventUpdated = (updatedEvent) => {
        setEvents((prevEvents) =>
            prevEvents.map(event => (event.event_id === updatedEvent.event_id ? updatedEvent : event))
        );
        setEditingEvent(null);
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div style={{ flex: '1' }}>
                <CreateEventForm onEventCreated={handleEventCreated} />
                {editingEvent && (
                    <EditEventForm
                        event={editingEvent}
                        onSave={handleEventUpdated}
                        onCancel={() => setEditingEvent(null)}
                    />
                )}
                <div className="events-list" style={{ overflowX: 'auto' }}>
                    <table style={{ width: '100%', borderCollapse: 'collapse' }}>
                        <thead>
                            <tr>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Event ID</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>User ID</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Title</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Picture</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Location</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Description</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Date</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Points</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {eventsList.map((event) => (
                                <tr key={event.event_id}>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.event_id}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.user_id}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.title}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>
                                        <img src={event.picture} alt={event.title} style={{ width: '100px', height: '100px' }} />
                                    </td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.location}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.description}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{new Date(event.date).toLocaleString()}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.points}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>
                                        <button onClick={() => handleDelete(event.event_id)}>Delete</button>
                                        <button onClick={() => handleEdit(event)}>Edit</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default EventsList;
