// @ts-check
import { useEffect, useState } from 'react'
// import { Navigate } from 'react-router-dom'
import { NavLink, useNavigate } from 'react-router-dom'
import useAuthService from '../../hooks/useAuthService'
import '../../App/stylesForm.css'


export default function SignInForm() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const { signin, user, error, setError } = useAuthService();
    const navigate = useNavigate();

    const handleCancelClick = () => {
        navigate('/');
    };

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    /**
     * @param {React.FormEvent<HTMLFormElement>} e
     */

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        await signin({ username, password }).finally(() => {
            setTimeout(() => {
                setError(undefined)
            }, 1500);
        });
    }
    useEffect(() => {
        if (user) {
            if (user.role === 'Student') {
                navigate("/studentDash");
            } else {
                navigate("/staffDash");
            }
        }
    }, [navigate, user]);


    return (
        <section style={{ backgroundColor: "white" }}>
            <div className="container py-5">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-xl-7">
                        <div className="card" style={{ borderRadius: "2rem" }} id='borderShadow'>
                            <div className="row g-0">
                                <div className="col-md-6">
                                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img1.webp" alt="login form" className="img-fluid" style={{ borderRadius: "2rem 0 0 2rem" }} />
                                </div>

                                <div className="col-md-3 col-lg-6 d-flex align-items-center">
                                    <div className='card-body'>

                                        <form className="form-container" onSubmit={handleFormSubmit}>
                                            {error && <div className="alert alert-warning">{error.message}</div>}

                                            <div className="d-flex align-items-center justify-content-center mb-1 pb-1">
                                                <div className="text-center">
                                                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/lotus.webp" style={{ width: "185px" }} alt="logo" />
                                                </div>
                                            </div>
                                            <h6 className="fw-normal mb-2 pb-3" style={{ letterSpacing: 1 }}>Sign into your account</h6>
                                            <div className="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    name="username"
                                                    value={username}
                                                    onChange={handleUsernameChange}
                                                    id="username"
                                                    placeholder="Username"
                                                    required
                                                    className="form-control form-control-sm"
                                                />
                                            </div>
                                            <div className="form-outline mb-4">
                                                <input
                                                    type="password"
                                                    name="password"
                                                    value={password}
                                                    onChange={handlePasswordChange}
                                                    id="password"
                                                    placeholder="Enter your password"
                                                    required
                                                    className="form-control form-control-sm"
                                                />
                                            </div>
                                            <div className="d-grid gap-5 d-md-flex justify-content-md-end">
                                                <button className="btn btn-dark" type="submit">Login</button>
                                                <button className="btn btn-secondary" type="button" onClick={handleCancelClick} >Cancel</button>
                                            </div>

                                            <NavLink style={{ color: "#393f81" }} to="/forgotPass">Forgot password?</NavLink>
                                            <p>{"Don't have an account?"} {' '}
                                                <NavLink style={{ color: "#393f81" }} to="/signUp">Register here</NavLink>
                                            </p>
                                            <div >
                                                <a href="#!" >Terms of use.</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
