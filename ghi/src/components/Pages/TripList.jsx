import React, { useState, useEffect } from 'react';
import '../../App/TripList.css'

function TripList({ tripUpdated }) {
    const [trips, setTrips] = useState([]);
    const [error, setError] = useState(null);
    const [editingTripId, setEditingTripId] = useState(null);
    const [editFormData, setEditFormData] = useState({
        title: '',
        picture: '',
        location: '',
        description: '',
        date: '',
        trip_type: '',
        status: '',
        last_updated: ''
    });

    useEffect(() => {
        async function fetchTrips() {
            try {
                const response = await fetch('http://localhost:8000/trips');
                if (!response.ok) throw new Error('Failed to fetch trips');
                const data = await response.json();
                setTrips(data);
            } catch (error) {
                setError(error.message);
            }
        }

        fetchTrips();

    }, [tripUpdated]);

    const handleDelete = async (id) => {
        try {
            const response = await fetch(`http://localhost:8000/trips/${id}`, {
                method: 'DELETE',
            });
            if (!response.ok) throw new Error('Failed to delete trip');
            setTrips(trips.filter(trip => trip.trip_id !== id));
        } catch (error) {
            setError(error.message);
        }
    };

    const handleEdit = (tripId) => {
        const tripToEdit = trips.find(trip => trip.trip_id === tripId);
        setEditingTripId(tripId);
        setEditFormData({
            title: tripToEdit.title,
            picture: tripToEdit.picture,
            location: tripToEdit.location,
            description: tripToEdit.description,
            date: tripToEdit.date,
            trip_type: tripToEdit.trip_type,
            status: tripToEdit.status,
            last_updated: tripToEdit.last_updated
        });
    };

    const handleSave = async (id) => {
        try {
            const url = `http://localhost:8000/trips/${id}`;
            const fetchOptions = {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(editFormData),
            };
            const response = await fetch(url, fetchOptions);
            if (!response.ok) throw new Error('Failed to update trip');
            const updatedTrip = await response.json();
            setTrips(trips.map(trip => trip.trip_id === id ? updatedTrip : trip));
            setEditingTripId(null);
        } catch (error) {
            setError(error.message);
        }
    };

    const handleCancel = () => {
        setEditingTripId(null);
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setEditFormData(prevFormData => ({
            ...prevFormData,
            [name]: value
        }));
    };

    const fieldsToEdit = [
        { name: 'title', label: 'Title', type: 'text' },
        { name: 'picture', label: 'Picture URL', type: 'text' },
        { name: 'location', label: 'Location', type: 'text' },
        { name: 'description', label: 'Description', type: 'textarea' },
        { name: 'date', label: 'Date', type: 'date' },
        { name: 'trip_type', label: 'Trip Type', type: 'text' },
        { name: 'status', label: 'Status', type: 'text' },
    ];

    return (
        <div className="page-wrapper2">
        <div className="trip-wrapper pfy-2">
            <h2 className="pfx-3 pfy-1">Trip List</h2>
            {error && <div className="error">{error}</div>}
            <ul className="pfx-3">
                {trips.map(trip => (
                    <div key={trip.trip_id}>
                        {editingTripId === trip.trip_id ? (
                            <div>
                                {fieldsToEdit.map(field => (
                                    <div className="pfy-1 px-1" key={field.name}>
                                        {field.type === 'textarea' ? (
                                            <textarea
                                                name={field.name}
                                                value={editFormData[field.name]}
                                                onChange={handleChange}
                                            />
                                        ) : (
                                            <input
                                                type={field.type}
                                                name={field.name}
                                                value={editFormData[field.name]}
                                                onChange={handleChange}
                                            />
                                        )}
                                    </div>
                                ))}
                                <div className="button-align">
                                    <div className="pfx-1 pfy-1">
                                        <button onClick={() => handleSave(trip.trip_id)}>Save</button>
                                    </div>
                                    <div className="pfx-1 pfy-1">
                                        <button onClick={handleCancel}>Cancel</button>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div className="list-setup">
                                <table className="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Picture</th>
                                            <th>Location</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Trip Type</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{trip.title}</td>
                                            <td>
                                                <img src={trip.picture} alt="Trip Preview" style={{ maxWidth: '250px', minWidth: '250px', paddingLeft: '10px' }} />
                                            </td>
                                            <td>{trip.description}</td>
                                            <td>{trip.location}</td>
                                            <td>{trip.trip_type}</td>
                                            <td>{trip.status}</td>
                                            <td>
                                                <div className="button-align">
                                                    <div className="px-1">
                                                        <button onClick={() => handleEdit(trip.trip_id)}>Edit</button>
                                                    </div>
                                                    <div className="px-1">
                                                        <button onClick={() => handleDelete(trip.trip_id)}>Delete</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        )}
                    </div>
                ))}
            </ul>
        </div>
    </div>
    );
}

export default TripList;
