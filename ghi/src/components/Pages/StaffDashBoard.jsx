import { NavLink } from 'react-router-dom'
import TripList from './TripList.jsx'
import useAuthService from '../../hooks/useAuthService'

export default function StaffDash() {
    const { user } = useAuthService();
    const handlePrint = () => {
        window.print();
    }

    return (
        <div className="container-fluid">
            <div className="row">

                <main className="col-md-10 ms-sm-auto col-lg-10 px-md-1">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 className="h2">Welcome: {user.username}</h1>
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group me-2">
                                <button type="button" className="btn btn-sm btn-outline-secondary" onClick={handlePrint} >Print 🖨️</button>
                                <button type="button" className="btn btn-sm btn-outline-secondary">Export</button>
                            </div>
                        </div>
                    </div>
                    <div style={{ overflowY: "scroll", maxHeight: '600px' }}>
                        <TripList />
                    </div>

                </main>

                <nav id="sidebarMenu" className="col-md-3 col-lg-2 bg-light d-md-block sidebar collapse">
                    <div className="position-sticky pt-3 sidebar-sticky">
                        <ul className="nav flex-column">
                            <li className="nav-item text-center">
                                <span data-feather="home" className="align-text-center" />
                                Admin Options:
                                <hr />
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/create-trip">
                                    Create Trip
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/events">
                                    Create Event
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="#">
                                    <span data-feather="users" className="align-text-bottom" />
                                    Students Registry
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="#">
                                    <span data-feather="bar-chart-2" className="align-text-bottom" />
                                    Students Point
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                    <span data-feather="layers" className="align-text-bottom" />
                                    Report
                                </a>
                            </li>
                        </ul>

                        <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted text-uppercase">
                            <span>Saved reports</span>
                            <a className="link-secondary" href="#" aria-label="Add a new report">
                                <span data-feather="plus-circle" className="align-text-bottom" />
                            </a>
                        </h6>
                    </div>
                </nav>

            </div>
        </div>
    );
}
