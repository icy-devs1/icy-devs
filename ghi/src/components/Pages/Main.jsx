import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App/JoshuaStyles.css'
import '../../App/Carousel.css'
import React, { useState, useEffect } from 'react';
import Trip1 from '../Images/Trip1.jpg'
import Trip2 from '../Images/Trip2.jpg'
import Trip3 from '../Images/Trip3.jpg'
import Trip4 from '../Images/Trip4.jpg'
import Trip5 from '../Images/Trip5.jpg'
import Trip6 from '../Images/Trip6.jpg'
import Trip7 from '../Images/Trip7.jpg'
import Trip8 from '../Images/Trip8.jpg'
import Trip9 from '../Images/Trip9.jpg'
import StockTrip from '../Images/StockTrip.jpg'

function Portal() {

    const [getTrips, setTrip] = useState([]);

    const tripGet = async () => {
        const response = await fetch('http://localhost:8000/trips');
        if (response.ok) {
            const data = await response.json();
            setTrip(data);
        }
        else {
            console.error('Error');
        }
    };

    useEffect(() => {
        tripGet();
    }, []);

    const HighlightMapping = () => {
        let returnData = getTrips.filter(test => {
            if (test.trip_type === "Highlight") {
                return test;
            }
        });

        if (!returnData.length) {
            returnData = [{
                "trip_id": 0,
                "title": "No Highlight Found",
                "picture": StockTrip,
                "description": "Unable to retrieve data. If the problem persists, please alert an ICY representative to resolve the issue."
            }];
        }

        return returnData;
    };

    const DreamMapping = () => {
        const today = new Date().toISOString();
        const isToday = today.substring(0, 10);
        let returnData = getTrips.filter(test => {
            if ((test.date.substring(0, 10) > isToday) && (test.trip_type === "Dream")) {
                return test;
            }
        });

        if (!returnData.length) {
            returnData = [{
                "trip_id": 0,
                "title": "Still Deciding!",
                "location": "Something",
                "description": "We will have the details announced shortly, and thank you so much for your patience."
            }];
        }

        return returnData;
    };

    return (
        <div className="page-wrapper1">
            <div className="events-wrapper" style={{ padding: '32px 48px' }}>
                <div className="events-actions">
                    <div className="gallery-2 ps-1">
                        <div className="gallery-container">
                            <div className="slider-container">
                                <div className="slider">
                                    <div className="slide-track">
                                        <div className="slide psx-1">
                                            <img src={Trip1} alt="Test1" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip2} alt="Test2" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip3} alt="Test3" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip4} alt="Test4" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip5} alt="Test5" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip6} alt="Test6" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip7} alt="Test7" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip1} alt="Test1" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip2} alt="Test2" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip3} alt="Test3" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip4} alt="Test4" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip5} alt="Test5" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip6} alt="Test6" className="image-box-gallery" />
                                        </div>
                                        <div className="slide psx-1">
                                            <img src={Trip7} alt="Test7" className="image-box-gallery" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="trip-2">
                        <div className="trip-container">
                            <div className="trip-items">
                                {getTrips && getTrips.length ?
                                    HighlightMapping().map((test2, index) => {
                                        if (test2.test_id !== 0 && index == 0) {
                                            return (
                                                <React.Fragment key={'my-key'}>
                                                    {
                                                        <div className="trip-items2">
                                                            <div className="left-box">
                                                                <img src={test2.picture} alt="Test1" className="image-box" />
                                                            </div>
                                                            <div className="right-box">
                                                                <div className="right-content">
                                                                    <div className="test right-top">
                                                                        <div className="right-details">
                                                                            {test2.title}
                                                                        </div>
                                                                        <div className="right-details2 pfy-1">
                                                                            <div>Located at {test2.location}</div>
                                                                            <p>on {test2.date}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="test right-bottom">
                                                                        <p className="right-details3 px-3">
                                                                            {test2.description}
                                                                        </p>
                                                                        <p className="right-details3 px-3">
                                                                            Last Updated: {test2.last_updated}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                </React.Fragment>
                                            );
                                        }
                                    })
                                    : null}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="sidebar-2">
                    <div className="sidebar-container">
                        <div className="sidebar-top">
                            <div className="next-up">
                                <div className="next-up3 px-3">Where We're Going Next...</div>
                            </div>
                        </div>
                        <div className="sidebar-middle sidebar-details">
                            {getTrips && getTrips.length ?
                                DreamMapping().map((test2, index) => {
                                    if (test2.test_id !== 0 && index == 0) {
                                        return (
                                            <React.Fragment key={'my-key'}>
                                                {
                                                    <div className="sidebar-middle2 sidebar-details">
                                                        <div className="test2 sidebar-top-box">
                                                            <div className="fitted-box">
                                                                <div className="px-1 py-2">
                                                                    <div className="top-text px-2">{test2.title}</div>
                                                                    <div className="bottom-text px-2 pt-1">{test2.location}</div>
                                                                </div>
                                                            </div>
                                                            <div className="fitted-box">
                                                                <div className="px-2 py-2" style={{ 'color': '#ff2255' }}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" fill="currentColor" className="bi bi-backpack2" viewBox="0 0 16 16">
                                                                        <path d="M4.04 7.43a4 4 0 0 1 7.92 0 .5.5 0 1 1-.99.14 3 3 0 0 0-5.94 0 .5.5 0 1 1-.99-.14" />
                                                                        <path fillRule="evenodd" d="M4 9.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5zm1 .5v3h6v-3h-1v.5a.5.5 0 0 1-1 0V10z" />
                                                                        <path d="M6 2.341V2a2 2 0 1 1 4 0v.341c2.33.824 4 3.047 4 5.659v1.191l1.17.585a1.5 1.5 0 0 1 .83 1.342V13.5a1.5 1.5 0 0 1-1.5 1.5h-1c-.456.607-1.182 1-2 1h-7a2.5 2.5 0 0 1-2-1h-1A1.5 1.5 0 0 1 0 13.5v-2.382a1.5 1.5 0 0 1 .83-1.342L2 9.191V8a6 6 0 0 1 4-5.659M7 2v.083a6 6 0 0 1 2 0V2a1 1 0 0 0-2 0M3 13.5A1.5 1.5 0 0 0 4.5 15h7a1.5 1.5 0 0 0 1.5-1.5V8A5 5 0 0 0 3 8zm-1-3.19-.724.362a.5.5 0 0 0-.276.447V13.5a.5.5 0 0 0 .5.5H2zm12 0V14h.5a.5.5 0 0 0 .5-.5v-2.382a.5.5 0 0 0-.276-.447L14 10.309Z" />
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="sidebar-bottom-box p-1">
                                                            <div className="top-text2 p-1">The Details</div>
                                                            <div className="bottom-text2 px-2">
                                                                <p>{test2.description}</p>
                                                                <div>Date: {test2.date}</div>
                                                                <p>
                                                                    If you have any questions, please reach out at least one week prior to the trip date to discuss any concerns with an ICY representative.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                            </React.Fragment>
                                        );
                                    }
                                })
                                : null}
                        </div>
                        <div className="sidebar-bottom">
                            <div className="next-up2">
                                <button className="test py-1 px-2 custom-button">
                                    <a href="https://www.zeffy.com/en-US/donation-form/b349c133-08f7-4ba3-9eca-b75a62a9a96d">Donate</a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Portal;
