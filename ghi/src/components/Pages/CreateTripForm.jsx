
import React, { useState } from 'react';
import '../../App/CreateTrip.css'

const CreateTripForm = ({ onTripCreated }) => {
    const [formData, setFormData] = useState({
        title: '',
        picture: '',
        location: '',
        description: '',
        date: '',
        trip_type: '',
        status: 'pending',
        last_updated: ''
    });

    const [error, setError] = useState('');

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        if (!formData.title || !formData.picture || !formData.location || !formData.date || !formData.trip_type || !formData.status) {
            setError('All fields are required');
            return;
        }

        if (formData.picture.length > 1000) {
            setError('Image URL should be 1000 characters or less');
            return;
        } else {
            setError('');
        }

        const currentDate = new Date().toISOString().split('T')[0];
        const dataToSubmit = { ...formData, last_updated: currentDate };

        const url = 'http://localhost:8000/trips';
        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataToSubmit),
        };

        try {
            const response = await fetch(url, fetchOptions);
            if (response.ok) {
                const newTrip = await response.json();
                setFormData({
                    title: '',
                    picture: '',
                    location: '',
                    description: '',
                    date: '',
                    trip_type: '',
                    status: 'pending',
                    last_updated: ''
                });
                if (typeof onTripCreated === 'function') {
                    onTripCreated();
                }
                alert('Trip created successfully!');
            } else {
                console.error('Failed to create trip');
                setError('Failed to create trip');

            }
        } catch (error) {
            console.error('Error submitting form:', error);
            setError('Error submitting form');

        }
    };


    return (
        <div className="page-wrapper pfx-3 pfy-2">
            <div className="border-wrap pfy-2">
                <h2 className="form-container">Create a New Trip</h2>
                <div className="form-setup">
                    <form className="form-setup pfx-1" onSubmit={handleSubmit}>
                        <div className="form-container py-1">
                            <label htmlFor="title">Title</label>
                            <input
                                type="text"
                                name="title"
                                id="title"
                                value={formData.title}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="img-container">
                            <div className="form-container py-2">
                                <label htmlFor="picture">Picture URL</label>
                                <input
                                    type="text"
                                    name="picture"
                                    id="picture"
                                    value={formData.picture}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="form-container img-position2">
                                {error && <p style={{ color: 'red' }}>{error}</p>}
                                {formData.picture && !error && (
                                    <img
                                        src={formData.picture}
                                        alt="Trip Preview"
                                        style={{ maxWidth: '100px', marginLeft: '10px' }}
                                    />
                                )}
                            </div>
                        </div>
                        <div className="form-container py-2">
                            <label htmlFor="location">Location</label>
                            <input
                                type="text"
                                name="location"
                                id="location"
                                value={formData.location}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="form-container py-2">
                            <label htmlFor="description">Description</label>
                            <textarea
                                name="description"
                                id="description"
                                value={formData.description}
                                onChange={handleChange}
                            ></textarea>
                        </div>
                        <div className="form-container py-2">
                            <label htmlFor="date">Date</label>
                            <input
                                type="date"
                                name="date"
                                id="date"
                                value={formData.date}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="form-container py-2">
                            <label htmlFor="trip_type">Trip Type</label>
                            <select
                                name="trip_type"
                                id="trip_type"
                                value={formData.trip_type}
                                onChange={handleChange}
                            >
                                <option value="">Select Trip Type</option>
                                <option value="Highlight">Highlight Trip</option>
                                <option value="Dream">Dream Trip</option>
                            </select>
                        </div>

                        <input
                            type="hidden"
                            name="status"
                            value={formData.status}
                        />
                        <input
                            type="hidden"
                            name="last_updated"
                            value={formData.last_updated}
                        />
                        <div className="py-2">
                            <button type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default CreateTripForm;
