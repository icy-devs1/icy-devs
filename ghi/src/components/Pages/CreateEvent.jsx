import React, { useState } from 'react';

function CreateEventForm({ onEventCreated }) {
    const MAX_URL_LENGTH = 1000; // Define the maximum URL length
    const [formData, setFormData] = useState({
        user_id: '',
        title: '',
        picture: '',
        location: '',
        description: '',
        date: '',
        points: ''
    });
    const [error, setError] = useState('');

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === 'picture' && value.length > MAX_URL_LENGTH) {
            setError(`URL must be less than ${MAX_URL_LENGTH} characters.`);
        } else {
            setError('');
            setFormData({ ...formData, [name]: value });
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (formData.picture.length > MAX_URL_LENGTH) {
            setError(`URL must be less than ${MAX_URL_LENGTH} characters.`);
            return;
        }
        try {
            const response = await fetch('http://localhost:8000/events/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });
            if (response.ok) {
                const newEvent = await response.json();
                onEventCreated(newEvent);
                setFormData({
                    user_id: '',
                    title: '',
                    picture: '',
                    location: '',
                    description: '',
                    date: '',
                    points: ''
                });
            } else {
                throw new Error('Failed to create event');
            }
        } catch (error) {
            console.error('Error creating event:', error);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="create-event-form">
            <div className="form-group">
                <label>User ID:</label>
                <input type="text" name="user_id" value={formData.user_id} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label>Title:</label>
                <input type="text" name="title" value={formData.title} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label>Picture URL:</label>
                <input type="text" name="picture" value={formData.picture} onChange={handleChange} required />
                {error && <div style={{ color: 'red' }}>{error}</div>}
            </div>
            <div className="form-group">
                <label>Location:</label>
                <input type="text" name="location" value={formData.location} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label>Description:</label>
                <textarea name="description" value={formData.description} onChange={handleChange} required></textarea>
            </div>
            <div className="form-group">
                <label>Date:</label>
                <input type="datetime-local" name="date" value={formData.date} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label>Points:</label>
                <input type="number" name="points" value={formData.points} onChange={handleChange} required />
            </div>
            <button type="submit">Create Event</button>
        </form>
    );
}

export default CreateEventForm;
