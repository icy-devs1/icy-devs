import React from "react";
import { NavLink, useNavigate } from 'react-router-dom';
import '../../App/stylesForm.css'

export default function ForgotPassword() {
    const navigate = useNavigate();

    const handlePasswordResetClick = () => {
        navigate('/signUp');
    };

    const handleCancelClick = () => {
        navigate('/');
    };

    return (
        <section style={{ backgroundColor: "white" }}>
            <div className="container py-5 h-90">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-xl-7">
                        <div className="card" style={{ borderRadius: "2rem" }} id='borderShadow'>
                            <div className="row g-0">
                                <div className="col-md-6">
                                    <div className="register-section">
                                        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img1.webp" alt="login form" className="img-fluid" style={{ borderRadius: "2rem 0 0 2rem" }} />
                                    </div>
                                </div>

                                <div className="col-md-3 col-lg-6 d-flex align-items-center">
                                    <div className='card-body'>
                                        <form className="form-container" onSubmit={handlePasswordResetClick}>
                                            <div className="d-flex justify-content-center mb-1 pb-1">
                                                <div className="text-center">
                                                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/lotus.webp" style={{ width: "185px" }} alt="logo" />
                                                </div>
                                            </div>
                                            <h6 className="fw-normal mb-4 " style={{ letterSpacing: 1 }}>Password Reset:</h6>
                                            <div className="form-outline mb-4">
                                                <input
                                                    type="email"
                                                    id="form2Example17"
                                                    placeholder="Enter your Email address"
                                                    className="form-control form-control-sm" />
                                            </div>

                                            <div className="d-grid gap-5 mb-4 d-md-flex justify-content-md-end">
                                                <button className="btn-green" type="submit">Send</button>
                                                <button className="btn btn-secondary" type="button" onClick={handleCancelClick} >Cancel</button>
                                            </div>
                                            <p className="mb-4">Already have an account?{' '}
                                                <NavLink style={{ color: "#393f81" }} to="/signin" >Sign In</NavLink>
                                            </p>
                                            <div>
                                                <a href="#!" >Terms of use</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
