import React, { useState } from 'react';
import pic1 from '../images/Latorya.jpg';
import pic2 from '../images/Crystal.jpg';
import pic3 from '../images/Tanya.jpg';
import pic4 from '../images/Julian.jpg';


const data = [{
    img: pic1,
    title: "FOUNDER",
    name: "Latorya Walker",
    show: "I'm a globetrotter, community advocate, & a Clark Atlanta University alumna (where I took my first flight). I have nine years of experience working with middle and high school kids. My present job is in the technology sector",
    para: ", where I seek to diversify the workforce. InterCulturalYouth, also known as ICY or ICY Abroad, was founded to give underserved youth access to foreign travel. My long-term goal is to incorporate travel into the K–12 core curriculum to expose students to diverse cultures, careers, & perspectives.",
},
{
    img: pic2,
    title: "Executive Director",
    name: "Crystal Spears",
    show: "Public school educator for over 11  years. Currently in San Antonio  working as a Special Education Coordinator. I entered the profession through Teach for America and enjoyed  supporting students.",
    para: "I love working with  students and providing experiences  that inevitably impact their life  trajectories.",
},
{
    img: pic3,
    title: "Global Learning & Develepment Coordinator",
    name: "Tanya Farirayi",
    show: "Licensed Therapist with 9 years of  experience in the education and mental  health field. Currently work as Program  Manager",
    para: " for a software engineering immersive program. My professional  journey started in the classroom. I love  helping student discover their true potential as they navigate this world.",
},
{
    img: pic4,
    title: "Recruitment Chairman",
    name: "Julian Serracin",
    show: "Youth Development professional with over  10 years of experience working in the  Metro Atlanta area. Currently a Community Mobilization Program manager for the Georgia Depart of Public Health overseeing youth",
    para: "development programs across the state. I am highly  dedicated to providing opportunities to youth and communities in need.",
},
];


const Card = ({ img, title, name, show, para }) => {
    const [showMore, setShowMore] = useState(false);

    return (
        <div className="card" style={{ display: 'flex', minHeight: 'max-content', height: '90%', flexDirection: 'column', alignItems: 'center', width: '20em', padding: '12px 0', borderRadius: '2em' }}>
            <div style={{ width: '10em', height: '10em', borderRadius: '50%', border: '5px solid rgb(129, 109, 249)', padding: '3px' }}><img src={img} alt="Card image cap" style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '50%' }} /></div>
            <div className="card-body">
                <h4 className="card-title">{title}</h4>
                <h5 className="card-name text-primary">{name}</h5>
                <p className="card-text py-1" style={{ fontSize: "14px" }}>{show}
                    {showMore && para}
                </p>
            </div>
            <button className="btn btn-primary" style={{ alignSelf: 'start', marginLeft: '1rem' }} onClick={() => setShowMore(prev => !prev)}>Show {showMore ? 'less' : 'more'}</button>
        </div>
    )
}


export default function Teams() {
    return (
        <div className="container" style={{ display: 'flex', gap: '1em', padding: '16px' }}>
            {data.map(item =>
                <Card key={item.name} {...item} />
            )}
        </div>
    );
}
