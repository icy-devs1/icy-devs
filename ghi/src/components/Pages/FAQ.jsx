import { Link, NavLink } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

function FAQ() {
    return (
        <>
            <div className="accordion" id="accordiExample">
                <div className="accordion-item">
                    <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            1. Who can participate in ICY programs?
                        </button>
                    </h2>
                    <div id="collapseOne" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <strong>ICY programs are open to high school students in grades 9-12 and college students starting from their sophomore year across the United States. We also welcome adult mentors and volunteers who are not currently in college, who are passionate about empowering youth and supporting their personal and academic growth, to help serve the regions we support.</strong>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            2. How do I apply to participate in ICY programs?
                        </button>
                    </h2>
                    <div id="collapseTwo" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <strong>To apply for ICY programs, simply visit our website and complete the online application form. Be sure to check the application deadlines for the program you are interested in.</strong>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            3. What types of programs does ICY offer?
                        </button>
                    </h2>
                    <div id="collapseThree" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <strong>ICY offers a wide range of programs aimed at providing students with opportunities for personal and academic growth. Our programs include international trips, domestic trips, company visits, college tours, community service projects, and more.</strong>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            4. How much do ICY programs cost?
                        </button>
                    </h2>
                    <div id="collapseFour" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <strong>Program costs vary depending on the type of program and its duration. We offer scholarships and financial aid to ensure that our programs are accessible to all students, regardless of financial need.</strong>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            5. How are ICY programs funded?
                        </button>
                    </h2>
                    <div id="collapseFive" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <strong>ICY programs are funded through a combination of application fees, apparel sales, deposits for trips, fundraising efforts, and generous donations from supporters.</strong>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            6. How can I become a mentor or volunteer with ICY?
                        </button>
                    </h2>
                    <div id="collapseSix" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <strong>We welcome mentors and volunteers who are passionate about empowering youth and supporting their personal and academic growth. To learn more about volunteer opportunities and how to get involved, please visit our website and check the application page, or contact us directly.

                                For additional questions, please check out our Q&A Session <a href="https://www.youtube.com/watch?v=ItefYtxOirk" target="_blank" rel="noopener noreferrer">here</a>, or reach out to us at info@icyabroad.org.</strong>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default FAQ;
