import React from 'react';

const valuesImage = 'https://images.squarespace-cdn.com/content/v1/6376d5444249525888c854d0/5de7b76f-98cb-4519-9a5e-4855cc0b1a32/values.png?format=750w';
const rightImage = 'https://images.squarespace-cdn.com/content/v1/6376d5444249525888c854d0/eecc9bb6-e210-4051-b8fd-7b535dc48f51/39.jpg?format=1500w';

function About() {
    return (
        <div className="page-wrapper2">
            <div>
                <h2 className="pt-3" style={{ textAlign: 'center', fontSize: '32px' }}>About</h2>
            </div>
            <div className="p-3" style={{ height: '100%' }} >
            <div className="" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', textAlign: 'center', height: '100%' }}>
                <div className="p-3" style={{ flex: 1 }}>
                    <img src={valuesImage} alt="Values" style={{ maxWidth: '75%', height: 'auto' }} />
                    <h3 className="p-2"style={{ maxWidth: '90%' }}>Our Mission</h3>
                    <p className="px-2">
                        Founded in March 2021, InterCulturalYouth (ICY) seeks to expand educational opportunities,
                        cultivate global perspectives, strengthen communities, and potentially change career and life
                        trajectories of minorities and students in low economic communities. By providing access to
                        global trips of limitless discovery, youth will have an opportunity to participate in an
                        educational journey and complete a culminating reflection activity.
                    </p>
                    <p className="px-2">
                        Our mission is to empower youth through educational excursions abroad and cultural exchange to
                        explore diverse career paths, promote cultural exchange and become change makers and leaders in
                        their communities and in the world.
                    </p>
                </div>
                <div style={{ flex: 1, paddingLeft: '20px' }}>
                    <img src={rightImage} alt="Right Image" style={{ maxWidth: '90%', height: 'auto' }} />
                </div>
            </div>
            </div>
        </div>
    );
}

export default About;
