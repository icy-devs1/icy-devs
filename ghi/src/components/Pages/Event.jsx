import React, { useEffect, useState } from 'react';

function Event() {
    const [eventsList, setEvents] = useState([]);

    const fetchData = async () => {
        try {
            const url = 'http://localhost:8000/events/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setEvents(data);
            } else {
                throw new Error('Failed to fetch Events');
            }
        } catch (error) {
            console.error('Error fetching Events:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div style={{ flex: '1' }}>
                <div className="events-list" style={{ overflowX: 'auto' }}>
                    <table style={{ width: '100%', borderCollapse: 'collapse' }}>
                        <thead>
                            <tr>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Event ID</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>User ID</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Title</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Picture</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Location</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Description</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Date</th>
                                <th style={{ padding: '10px', border: '1px solid #ddd' }}>Points</th>
                            </tr>
                        </thead>
                        <tbody>
                            {eventsList.map((event) => (
                                <tr key={event.event_id}>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.event_id}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.user_id}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.title}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>
                                        <img src={event.picture} alt={event.title} style={{ width: '100px', height: '100px' }} />
                                    </td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.location}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.description}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{new Date(event.date).toLocaleString()}</td>
                                    <td style={{ padding: '10px', border: '1px solid #ddd' }}>{event.points}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Event;
