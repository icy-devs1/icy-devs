import { useState } from 'react'
import { NavLink, useNavigate } from 'react-router-dom';
import useAuthService from '../../hooks/useAuthService'
import '../../App/stylesForm.css'

export default function SignInForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const { signup, user, error, setError } = useAuthService()

    const navigate = useNavigate(); //new addition


    const handleCancelClick = () => {// come back to the main page
        navigate('/');
    };

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };
    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };
    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault();

        if (username === '' || password === '' || email === '') {
            setError(Error('Oops missing field was found, please fill before continuing.'));
        } else {

            const data = { username, password, email };
            await signup(data);

            if (!error) {
                setUsername('');
                setPassword('');
                setEmail('');
                navigate('/signin');
                return
            }

            setTimeout(() => {
                setError(undefined)
            }, 3000);

        }
    }

    // If the user is already authenticated, redirect to home page
    if (user) {
        handleCancelClick();
        return false;
    }

    return (
        <section style={{ backgroundColor: "white" }}>
            <div className="container py-5">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-xl-7">
                        <div className="card" style={{ borderRadius: "2rem" }} id='borderShadow'>
                            <div className="row g-0">
                                <div className="col-md-6">
                                    <div className="register-section">
                                        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img1.webp" alt="login form" className="img-fluid" style={{ borderRadius: "2rem 0 0 2rem" }} />
                                    </div>
                                </div>

                                <div className="col-lg-6 d-flex align-items-center">

                                    <div className="card-body text-black">

                                        <form className='form-container mb-3' onSubmit={handleFormSubmit}>
                                            {error && <div className="alert alert-danger" role="alert">{error.message}</div>}
                                            <div className="d-flex align-items-center justify-content-center mb-1 pb-1">
                                                <div className="text-center mb-3">
                                                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/lotus.webp" style={{ width: "185px" }} alt="logo" />
                                                </div>
                                            </div>
                                            <h6 className="fw-normal mb-4" style={{ letterSpacing: 1 }}>Create an Account:</h6>
                                            <div className="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    name="username"
                                                    value={username}
                                                    onChange={handleUsernameChange}
                                                    id="username"
                                                    required
                                                    placeholder="Username:"
                                                    className="form-control" />
                                            </div>
                                            <div className="form-outline mb-4">
                                                <input
                                                    type="password"
                                                    name="password"
                                                    value={password}
                                                    onChange={handlePasswordChange}
                                                    id="password"
                                                    required
                                                    placeholder="Password:"
                                                    className="form-control" />
                                            </div>

                                            <div className="input-group mb-4 w-75">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="inputGroup-sizing-default">@</span>
                                                </div>
                                                <input
                                                    name="email"
                                                    value={email}
                                                    onChange={handleEmailChange}
                                                    id="email"
                                                    required
                                                    className="form-control"
                                                    placeholder="Email address:"
                                                    aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                                            </div>
                                            <div className="d-grid gap-5 mb-3 d-md-flex justify-content-md-end">
                                                <button className="btn btn-dark" type="submit">Register</button>
                                                <button className="btn btn-secondary" type="button" onClick={handleCancelClick} >Cancel</button>
                                            </div>
                                            <p className="mb-1">Already have an account?{' '}
                                                <NavLink style={{ color: "#393f81" }} to="/signin">Sign In</NavLink>
                                            </p>
                                            <div>
                                                <a href="#!" >Terms of use.</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
