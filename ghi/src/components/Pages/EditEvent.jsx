import React, { useState, useEffect } from 'react';

const EditEventForm = ({ event, onSave, onCancel }) => {
    const [formData, setFormData] = useState({
        user_id: '',
        title: '',
        description: '',
        date: '',
        time: '',
        location: '',
        picture: '',
        points: ''
    });

    useEffect(() => {
        if (event) {
            setFormData({
                user_id: event.user_id,
                title: event.title,
                description: event.description,
                date: event.date.split('T')[0],
                time: event.date.split('T')[1].substr(0, 5),
                location: event.location,
                picture: event.picture,
                points: event.points
            });
        }
    }, [event]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const updatedEvent = {
            ...formData,
            date: new Date(`${formData.date}T${formData.time}`).toISOString(), // combine date and time
        };

        try {
            const url = `http://localhost:8000/events/${event.event_id}/`;
            const response = await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updatedEvent),
            });

            if (response.ok) {
                const savedEvent = await response.json();
                onSave(savedEvent);
            } else {
                throw new Error('Failed to save event');
            }
        } catch (error) {
            console.error('Error saving event:', error);
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="user_id">User_id:</label>
                <input
                    type="number"
                    id="user_id"
                    name="user_id"
                    value={formData.user_id}
                    onChange={handleChange}
                />
            </div>
            <div>
                <label htmlFor="title">Title:</label>
                <input
                    type="text"
                    id="title"
                    name="title"
                    value={formData.title}
                    onChange={handleChange}
                />
            </div>
            <div>
                <label htmlFor="description">Description:</label>
                <textarea
                    id="description"
                    name="description"
                    value={formData.description}
                    onChange={handleChange}
                ></textarea>
            </div>
            <div>
                <label htmlFor="date">Date:</label>
                <input
                    type="date"
                    id="date"
                    name="date"
                    value={formData.date}
                    onChange={handleChange}
                />
            </div>
            <div>
                <label htmlFor="time">Time:</label>
                <input
                    type="time"
                    id="time"
                    name="time"
                    value={formData.time}
                    onChange={handleChange}
                />
            </div>
            <div>
                <label htmlFor="location">Location:</label>
                <input
                    type="text"
                    id="location"
                    name="location"
                    value={formData.location}
                    onChange={handleChange}
                />
            </div>
            <div>
                <label htmlFor="picture">Picture URL:</label>
                <input
                    type="text"
                    id="picture"
                    name="picture"
                    value={formData.picture}
                    onChange={handleChange}
                />
            </div>
            <div>
                <label htmlFor="points">Points:</label>
                <input
                    type="number"
                    id="points"
                    name="points"
                    value={formData.points}
                    onChange={handleChange}
                />
            </div>
            <div>
                <button type="submit">Save</button>
                <button type="button" onClick={onCancel}>Cancel</button>
            </div>
        </form>
    );
};

export default EditEventForm;
