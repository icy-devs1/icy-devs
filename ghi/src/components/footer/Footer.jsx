import React from 'react';
import { NavLink } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FaFacebook, FaInstagram, FaEnvelope, FaTiktok, FaYoutube } from 'react-icons/fa';
import './Footer.css';

function Footer() {
  return (
    <footer>
      <section className="footer-1 footer-text position">
        <hr />
        <section className="footer-info p-2">
          <section className="footer-info-left pl-2">
            <section className="footer-info__name">
              <NavLink className="mr-2" to="/faq">FAQ</NavLink>
            </section>
          </section>
          <section className="footer-info-center">
            <section className="footer-info__terms pft-1">
              Terms and Conditions
              <br />
              Copyright
            </section>
          </section>
          <section className="footer-info-right pr-1">
            Contact Info:
            <a href="https://www.facebook.com/IcyAbroad" className="mr-2"><FaFacebook style={{ fontSize: '20px' }} /></a>
            <a href="https://www.instagram.com/icyabroad" className="mr-2"><FaInstagram style={{ fontSize: '20px' }} /></a>
            <a href="https://www.youtube.com/@icyabroad" className="mr-2"><FaYoutube style={{ fontSize: '20px' }} /></a>
            <a href="https://www.tiktok.com/@icyabroad" className="mr-2"><FaTiktok style={{ fontSize: '20px' }} /></a>
            <a href="mailto:info@icyabroad.org"><FaEnvelope style={{ fontSize: '20px' }} /></a>
          </section>
        </section>
      </section>
    </footer>
  )
}
export default Footer;
