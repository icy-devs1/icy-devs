import useAuthService from '../../hooks/useAuthService'
import { FiMenu } from 'react-icons/fi'
import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import '../../App/JoshuaStyles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import logo from '../Images/LogoICY.png';

function NavTest() {
    const { isLoggedIn } = useAuthService();
    const [getEvents, setEvent] = useState([]);
    const [query, setQuery] = useState("");


    const signOut = async () => {
        try {
            const response = await fetch('http://localhost:8000/api/auth/signout', {
                method: 'DELETE',
                credentials: 'include',
            })
            // Check if the response indicates successful signout
            if (response.ok) {
                // Redirect to the home page
                window.location.href = '/'
            } else {
                console.error('Failed to sign out:', response.statusText)
            }
        } catch (error) {
            console.error('Error signing out:', error)
        }
    }

    const eventGet = async () => {
        const response = await fetch('http://localhost:8000/events/');
        if (response.ok) {
            const data = await response.json();
            setEvent(data);
        }
        else {
            console.error('Error');
        }
    }

    useEffect(() => {
        eventGet();
    }, []);

    const EventMapping = () => {
        let returnData = getEvents.filter(test => {
            if (test.title.toLowerCase().includes(query.toLowerCase())) {
                return test;
            }
        });

        if (!returnData.length) {
            returnData = [{
                "event_id": 0,
                "title": "No Results Found"
            }];
        }

        return returnData;
    };

    return (
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
            <NavLink className="navbar-brand" to="/">
                <img src={logo} alt="Logo" className="pl-3 pr-2" />
            </NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="navbar collapse navbar-collapse" id="navbarScroll">
                <ul className="navbar-nav my-2 my-lg-0 navbar-nav-scroll" style={{ "--bs-scroll-height": "100px" }}>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/about">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/team">Team</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/event">Event</NavLink>
                    </li>
                </ul>
                <div className='navbar-end pr-1'>
                    <ul className="navbar-nav navbar-end" style={{ alignItems: "center", gap: '32px' }}>
                        <form className="navbar-end" role="search">
                            <input onChange={event => setQuery(event.target.value)} className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                            {
                                !query ?
                                    null
                                    :
                                    <div className="box">
                                        {
                                            getEvents && getEvents.length ?
                                                EventMapping().map((test2) => {
                                                    return (
                                                        <React.Fragment key={test2.event_id}>
                                                            {
                                                                test2.event_id !== 0 ?
                                                                    <div className="box-items" key={test2.event_id}>
                                                                        <NavLink to={`events/${test2.event_id}/`}>
                                                                            <div className="box-item py-1 px-3">{test2.title}</div>
                                                                        </NavLink>
                                                                    </div>
                                                                    :
                                                                    <div className="box-items" key={test2.event_id}>
                                                                        <div className="box-item p-1">{test2.title}</div>
                                                                    </div>
                                                            }
                                                        </React.Fragment>
                                                    )
                                                })
                                                :
                                                <div></div>
                                        }
                                    </div>
                            }
                            <button style={{ border: 'none', backgroundColor: 'none !important'}} type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001q.044.06.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1 1 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0" />
                            </svg></button>
                        </form>

                        <li className="nav-item dropdown pr-2">
                            <FiMenu role="button" data-bs-toggle="dropdown" />
                            <ul className="dropdown-menu dropdown-menu-end">
                                {isLoggedIn ?
                                    (<li><NavLink className="dropdown-item" onClick={signOut}>Logout</NavLink></li>)
                                    :
                                    (<li><NavLink className="dropdown-item" to="/signin">Login</NavLink></li>)
                                }
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}
export default NavTest;
