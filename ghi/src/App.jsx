import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import React, { useState, useEffect } from 'react';

// This makes VSCode check types as if you are using TypeScript
//@ts-check


// This makes VSCode check types as if you are using TypeScript
//@ts-check

import Nav from './components/Header/Nav'
import Footer from './components/footer/Footer'
import './App.css';

import Main from './components/Pages/Main';
import FAQ from './components/Pages/FAQ'
import Team from './components/Pages/Team'
import About from './components/Pages/About'
import Event from './components/Pages/Event'
import SignInForm from './components/Pages/SignInForm'
import ForgotPassword from './components/Pages/ForgotPasswordForm'
import SignUpForm from './components/Pages/SignUpForm'
import CreateTripForm from './components/Pages/CreateTripForm'
import TripList from './components/Pages/TripList'
import EventsList from './components/Pages/EventsList'
import StaffDashBoardForm from './components/Pages/StaffDashBoard'
import StudentDashBoardForm from './components/Pages/StudentDashBoard'

function App() {

    return (
        <BrowserRouter>
            <div className="main-wrapper">
                <Nav />
                <div className="workspace">
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <Routes>
                        <Route path="/" element={<Main />} />
                        <Route path="/faq" element={<FAQ />} />
                        <Route path="/about" element={<About />} />
                        <Route path="/team" element={<Team />} />
                        <Route path="/event" element={<Event />} />
                        <Route path="/signin" element={<SignInForm />} />
                        <Route path="/signUp" element={<SignUpForm />} />
                        <Route path="/forgotPass" element={<ForgotPassword />} />
                        <Route path="/create-trip" element={<CreateTripForm />} />
                        <Route path="/trips" element={<TripList />} />
                        <Route path="/events" element={<EventsList />} />
                        <Route path="/studentDash" element={<StudentDashBoardForm />} />
                        <Route path="/staffDash" element={<StaffDashBoardForm />} />
                    </Routes>
                </div>
                <Footer />
                </div>
            </div>
        </BrowserRouter>
    )
}
export default App;
