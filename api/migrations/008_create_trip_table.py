steps = [
    [
        """
        CREATE TABLE Trips (
            trip_id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(255) NOT NULL,
            picture VARCHAR(255) NOT NULL,
            description VARCHAR(1000) NOT NULL,
            location VARCHAR(255) NOT NULL,
            date DATE NOT NULL,
            trip_type VARCHAR(255) NOT NULL,
            status VARCHAR(255) NOT NULL,
            last_updated DATE NOT NULL
        );
        """,
        """
        DROP TABLE Trips;
        """,
    ]
]
