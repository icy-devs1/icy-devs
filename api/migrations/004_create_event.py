steps = [
    [
        # "Up" SQL statement
        """
            CREATE TABLE Event(
                event_id SERIAL PRIMARY KEY NOT NULL,
                user_id INTEGER,
                title VARCHAR(255),
                picture VARCHAR(255) NOT NULL,
                location VARCHAR(255) NOT NULL,
                description VARCHAR(1000) NOT NULL,
                date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                points INTEGER NOT NULL
            );
            """,
        # "Down" SQL statement
        """,
            DROP TABLE Event;
            """,
    ]
]
