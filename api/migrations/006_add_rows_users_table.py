steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE users
            ADD first_name VARCHAR(50) NULL,
            ADD last_name VARCHAR(50) NULL,
            ADD email VARCHAR(100) NULL,
            ADD date_birth DATE NULL,
            ADD phone_number VARCHAR(255) NULL,
            ADD address VARCHAR(255) NULL,
            ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT now(),
            ADD points INTEGER NULL DEFAULT 0,
            ADD role VARCHAR(255) DEFAULT 'Student' NOT NULL
        """,
        # "Down" SQL statement
        """
        DROP COLUMN user_id;
        """,
    ]
]
