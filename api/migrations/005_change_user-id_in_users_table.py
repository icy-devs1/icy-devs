steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE users
        RENAME COLUMN id TO user_id;
        """,
        # "Down" SQL statement
        """
        DROP COLUMN user_id;
        """,
    ]
]
