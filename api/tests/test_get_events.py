from queries.events_queries import EventsQueries
from fastapi.testclient import TestClient
from fastapi import status
from main import app

# from models.events import EventsOut

client = TestClient(app)


class MockQuery(EventsQueries):
    def get_events(self):
        return [
            {
                "event_id": 2,
                "user_id": 4,
                "title": "Test Case Trip",
                "picture": "url2",
                "location": "Test Island",
                "description": "Don't worry about the details.",
                "date": "2025-07-22 00:00:00",
                "points": -5,
            }
        ]


def test_get_events():
    app.dependency_overrides[EventsQueries] = MockQuery
    expected = [
        {
            "event_id": 2,
            "user_id": 4,
            "title": "Test Case Trip",
            "picture": "url2",
            "location": "Test Island",
            "description": "Don't worry about the details.",
            "date": "2025-07-22T00:00:00",
            "points": -5,
        }
    ]
    response = client.get("/events/")
    app.dependency_overrides = {}
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
