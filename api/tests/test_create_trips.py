from queries.trips_queries import TripsQueries
from fastapi.testclient import TestClient
from fastapi import status
from main import app

client = TestClient(app)


class MockTripsQueries(TripsQueries):
    def create_trip(self, trip):
        return {
            "trip_id": 1,
            "title": trip.title,
            "picture": trip.picture,
            "location": trip.location,
            "description": trip.description,
            "date": str(trip.date),
            "trip_type": trip.trip_type,
            "status": trip.status,
            "last_updated": str(
                trip.last_updated if trip.last_updated else trip.date
            ),
        }


def test_create_trip():
    app.dependency_overrides[TripsQueries] = MockTripsQueries
    trip_data = {
        "title": "Test Trip",
        "picture": "url",
        "location": "Test Location",
        "description": "Test Description",
        "date": "2025-07-22",
        "trip_type": "Test Type",
        "status": "Active",
        "last_updated": "2024-09-20",
    }
    expected = {
        "trip_id": 1,
        "title": "Test Trip",
        "picture": "url",
        "location": "Test Location",
        "description": "Test Description",
        "date": "2025-07-22",
        "trip_type": "Test Type",
        "status": "Active",
        "last_updated": "2024-09-20",
    }

    response = client.post("/trips", json=trip_data)
    app.dependency_overrides = {}
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
