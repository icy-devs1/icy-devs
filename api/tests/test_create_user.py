from main import app
from fastapi.testclient import TestClient
from queries.user_queries import UserQueries
from models.users import UserWithPw
from fastapi import status

client = TestClient(app)


class MockCreateUserQueries:
    def create_user(self, username, hashed_password, email):
        user = UserWithPw(
            user_id=16,
            username=username,
            password=hashed_password,
            email=email,
            created_at="2024-06-22T00:59:37",
            points=0,
            role="Student",
        )
        return user


def test_create_user():
    app.dependency_overrides[UserQueries] = MockCreateUserQueries
    json = {
        "user_id": 16,
        "username": "nancy_black",
        "password": "string",
        "email": "nancy.black@example.com",
    }
    expected = {
        "user_id": 16,
        "username": "nancy_black",
        "email": "nancy.black@example.com",
        "created_at": "2024-06-22T00:59:37",
        "points": 0,
        "role": "Student",
    }

    response = client.post("api/auth/signup/", json=json)

    app.dependency_overrides = {}

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
