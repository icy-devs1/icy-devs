from fastapi.testclient import TestClient
from fastapi import status
from main import app
from queries.events_queries import EventsQueries


client = TestClient(app)


class GetEventDetail(EventsQueries):
    def get_event(self, event_id):
        event_id = 1
        result = {
            "event_id": event_id,
            "user_id": 4,
            "title": "string",
            "picture": "string",
            "location": "string",
            "description": "string",
            "date": "1985-08-22 00:00:00",
            "points": 5,
        }

        return result


def test_get_event():
    app.dependency_overrides[EventsQueries] = GetEventDetail
    event_id = 1
    expected = {
        "event_id": event_id,
        "user_id": 4,
        "title": "string",
        "picture": "string",
        "location": "string",
        "description": "string",
        "date": "1985-08-22T00:00:00",
        "points": 5,
    }

    response = client.get("/events/1/")
    app.dependency_overrides = {}
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
