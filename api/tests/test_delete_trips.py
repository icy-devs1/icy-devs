from fastapi.testclient import TestClient
from main import app
from queries.trips_queries import TripsQueries
from fastapi import status

client = TestClient(app)


class MockQuery(TripsQueries):
    def delete_trip(self, trip_id: int):
        if trip_id == 5:
            return None


def test_delete_trip_success():
    app.dependency_overrides[TripsQueries] = MockQuery
    expected = None
    response = client.delete("/trips/5")
    app.dependency_overrides = {}
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
