from fastapi.testclient import TestClient
from main import app
from queries.user_queries import UserQueries
from fastapi import status


client = TestClient(app)


# Mock class for overriding UserQueries dependency
class DeleteUserQueries(UserQueries):
    def delete_user(self, username: str):
        if username == "john_doe":
            return {"message": "User 'john_doe' was deleted successfully."}
        else:
            raise Exception("User 'john_doe' not found")


def test_delete_user_success():
    # Override the UserQueries dependency with DeleteUserQueries mock class
    app.dependency_overrides[UserQueries] = DeleteUserQueries

    # Expected response from the API
    expected = {"message": "User 'john_doe' was deleted successfully."}

    # Send a DELETE request to the endpoint
    response = client.delete("/users/john_doe")

    # Clean up the dependency override
    app.dependency_overrides = {}

    # Assert the response status code and JSON content
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
