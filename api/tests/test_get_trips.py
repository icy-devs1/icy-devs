from fastapi.testclient import TestClient
from fastapi import status
from main import app
from queries.trips_queries import TripsQueries

client = TestClient(app)


class MockTripsQueries(TripsQueries):
    def get_all_trips(self):
        return [
            {
                "trip_id": 1,
                "title": "Test Trip 1",
                "picture": "url1",
                "location": "Location 1",
                "description": "Description 1",
                "date": "2025-07-22",
                "trip_type": "Type 1",
                "status": "Active",
                "last_updated": "2024-09-20",
            },
            {
                "trip_id": 2,
                "title": "Test Trip 2",
                "picture": "url2",
                "location": "Location 2",
                "description": "Description 2",
                "date": "2025-07-23",
                "trip_type": "Type 2",
                "status": "Inactive",
                "last_updated": "2024-09-21",
            },
        ]


def test_get_all_trips():
    app.dependency_overrides[TripsQueries] = MockTripsQueries

    expected = [
        {
            "trip_id": 1,
            "title": "Test Trip 1",
            "picture": "url1",
            "location": "Location 1",
            "description": "Description 1",
            "date": "2025-07-22",
            "trip_type": "Type 1",
            "status": "Active",
            "last_updated": "2024-09-20",
        },
        {
            "trip_id": 2,
            "title": "Test Trip 2",
            "picture": "url2",
            "location": "Location 2",
            "description": "Description 2",
            "date": "2025-07-23",
            "trip_type": "Type 2",
            "status": "Inactive",
            "last_updated": "2024-09-21",
        },
    ]

    response = client.get("/trips")

    app.dependency_overrides.clear()

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
