from fastapi.testclient import TestClient
from main import app
from queries.events_queries import EventsQueries
from fastapi import status

client = TestClient(app)


class MockQuery(EventsQueries):
    def delete_event(self, event_id: int):
        if event_id == 5:
            return None


def test_delete_event_success():
    app.dependency_overrides[EventsQueries] = MockQuery
    expected = None
    response = client.delete("/events/5")
    app.dependency_overrides = {}
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
