from models.users import UserDetailIn, UserDetailOut
from queries.user_queries import UserQueries
from utils.exceptions import UserDatabaseException

# from typing import List
from models.users import UserResponse

# from queries.user_queries import UserQueries
from fastapi import (
    Depends,
    Response,
    APIRouter,
    HTTPException,
    status,
)

router = APIRouter()


@router.put("/users/update", response_model=UserDetailOut)
def edit_user(
    username: str,
    user_in: UserDetailIn,
    response: Response,
    queries: UserQueries = Depends(),
):
    record = queries.edit_user(username, user_in)
    if record is None:
        response.status_code = 404
    else:
        return record


# @router.get("/users/{user_id}", response= UserOut)
# async def get_user(user_id: int,queries: UserQueries = Depends()):
#     user = queries.get_by_id(user_id)
#     if user is None:
#         response.status_code = 404
#     return user


@router.get("/users/{user_id}", response_model=UserResponse)
async def get_user(
    user_id: int,
    response: Response,
    queries: UserQueries = Depends(),
):
    user = queries.get_by_id(user_id)
    if user is None:
        response.status_code = 404
    return user


@router.delete("/users/{username}")  # delete by username
async def delete_user(username: str, queries: UserQueries = Depends()):
    try:
        queries.delete_user(username)
        return {"message": f"User '{username}' was deleted successfully."}
    except UserDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=str(e)
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Failed to delete user '{username}'. Error: {str(e)}",
        )
