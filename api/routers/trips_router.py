from fastapi import APIRouter, Depends, Response
from models.trips import TripsIn, TripsOut
from queries.trips_queries import TripsQueries

router = APIRouter()


@router.post("/trips", response_model=TripsOut)
def create_trip(
    trip: TripsIn,
    queries: TripsQueries = Depends(),
):
    result = queries.create_trip(trip)
    return result


@router.get("/trips/{trip_id}", response_model=TripsOut)
def get_trip_details(
    trip_id: int,
    queries: TripsQueries = Depends(),
):
    trip = queries.get_trip_details(trip_id)
    if not trip:
        return Response(status_code=404)
    return trip


@router.put("/trips/{trip_id}", response_model=TripsOut)
def edit_trip(
    trip_id: int,
    trip: TripsIn,
    response: Response,
    queries: TripsQueries = Depends(),
):
    record = queries.edit_trip(trip_id, trip)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.get("/trips", response_model=list[TripsOut])
def get_all_trips(queries: TripsQueries = Depends()):
    return queries.get_all_trips()


@router.delete("/trips/{trip_id}")
def delete_trip(
    trip_id: int,
    queries: TripsQueries = Depends(),
):
    queries.delete_trip(trip_id)
