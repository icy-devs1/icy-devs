from typing import List
from fastapi import APIRouter, Depends, Response
from models.events import EventsIn, EventsOut
from queries.events_queries import EventsQueries

router = APIRouter()


@router.put("/events/{event_id}/", response_model=EventsOut)
def edit_event(
    event_id: int,
    event_in: EventsIn,
    response: Response,
    queries: EventsQueries = Depends(),
):
    record = queries.edit_event(event_id, event_in)
    if record is None:
        response.status_code = 404
    else:
        return record


# Create a new event
@router.post("/events/", response_model=EventsOut)
def create_event(
    event: EventsIn,
    queries: EventsQueries = Depends(),
):
    result = queries.create_event(event)
    return result


@router.delete("/events/{event_id}")
def delete_event(
    event_id: int,
    queries: EventsQueries = Depends(),
):
    queries.delete_event(event_id)


@router.get("/events/", response_model=List[EventsOut])
def get_events(events_queries: EventsQueries = Depends()):
    events = events_queries.get_events()
    return events


@router.get("/events/{event_id}/", response_model=EventsOut)
def get_event(event_id: int, events_queries: EventsQueries = Depends()):
    event = events_queries.get_event(event_id)
    return event
