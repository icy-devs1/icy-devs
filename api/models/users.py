"""
Pydantic Models for Users.
"""

import datetime
from pydantic import BaseModel


class UserRequest(BaseModel):
    """
    Represents a the parameters needed to create a new user
    """

    username: str
    password: str
    email: str


class UserResponse(BaseModel):
    """
    Represents a user, with the password not included
    """

    user_id: int
    username: str
    email: str
    created_at: datetime.datetime
    points: int
    role: str


class UserWithPw(BaseModel):
    """
    Represents a user with password included
    """

    user_id: int
    username: str
    password: str
    email: str
    created_at: datetime.datetime
    points: int
    role: str


class UserDetailIn(BaseModel):
    username: str
    password: str
    first_name: str
    last_name: str
    email: str
    date_birth: datetime.date
    phone_number: str
    address: str
    created_at: datetime.datetime
    points: int
    role: str


class UserDetailOut(BaseModel):
    email: str
    email: str
    created_at: datetime.datetime
    points: int
    role: str


# for signin part request
class UserRequestPw(BaseModel):
    username: str
    password: str


class UserIn(BaseModel):
    username: str
    password: str
    email: str
    points: int
    created_at: datetime.datetime


class UserOut(BaseModel):
    user_id: int
    username: str
    password: str
    first_name: str
    last_name: str
    email: str
    date_birth: datetime.datetime
    phone_number: str
    address: str
    created_at: datetime.datetime
    points: int
    role: str
