from pydantic import BaseModel
import datetime
from typing import Optional


class TripsIn(BaseModel):
    title: str
    picture: str
    location: str
    description: str
    date: datetime.date
    trip_type: str
    status: str
    last_updated: Optional[datetime.date] = None


class TripsOut(BaseModel):
    trip_id: int
    title: str
    picture: str
    location: str
    description: str
    date: datetime.date
    trip_type: str
    status: str
    last_updated: datetime.date
