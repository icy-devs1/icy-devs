from pydantic import BaseModel
import datetime


class EventsIn(BaseModel):
    user_id: int
    title: str
    picture: str
    location: str
    description: str
    date: datetime.datetime
    points: int


class EventsOut(BaseModel):
    event_id: int
    user_id: int
    title: str
    picture: str
    location: str
    description: str
    date: datetime.datetime
    points: int
