"""
Database Queries for Users and Events
"""

import os
import psycopg
from psycopg.rows import class_row
from models.events import EventsOut  # Importing EventsOut model
from psycopg_pool import ConnectionPool
from fastapi import HTTPException
from typing import Optional
from utils.exceptions import UserDatabaseException

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class EventsQueries:

    def create_event(self, event) -> EventsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    params = [
                        event.user_id,
                        event.title,
                        event.picture,
                        event.location,
                        event.description,
                        event.date,
                        event.points,
                    ]
                    cur.execute(
                        """
                        INSERT INTO event(
                        user_id,title,picture,location,description,date,points
                        ) VALUES(
                            %s,%s,%s,%s,%s,%s,%s
                            )
                        RETURNING  *;
                        """,
                        params,
                    )
                    row = cur.fetchone()
                    if row is None:
                        raise HTTPException(
                            status_code=500, detail="Failed to create event"
                        )

                    return EventsOut(
                        event_id=row[0],
                        user_id=row[1],
                        title=row[2],
                        picture=row[3],
                        location=row[4],
                        description=row[5],
                        date=row[6],
                        points=row[7],
                    )
        except Exception as e:
            print(f"An error occurred: {e}")
            raise HTTPException(
                status_code=500,
                detail="An error occurred while creating the event",
            )

    def delete_event(self, event_id: int) -> Optional[EventsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(EventsOut)) as cur:
                    cur.execute(
                        """
                        DELETE FROM event
                        WHERE event_id = %s;
                        """,
                        [event_id],
                    )
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(
                f"Error deleting event with id {event_id}"
            )

    def edit_event(self, event_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.user_id,
                    data.title,
                    data.picture,
                    data.location,
                    data.description,
                    data.date,
                    data.points,
                    event_id,
                ]
                cur.execute(
                    """
                        UPDATE event
                        SET (user_id
                            , title
                            , picture
                            , location
                            , description
                            , date
                            , points)
                            = (%s,%s,%s,%s,%s,%s,%s)
                        WHERE event_id = %s
                        RETURNING user_id
                        , title
                        , picture
                        , location
                        , description
                        , date
                        , points
                        , event_id
                        """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

            return record

    def get_events(self):
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(EventsOut)) as cur:
                    cur.execute(
                        """
                        SELECT
                            event_id,
                            user_id,
                            title,
                            picture,
                            location,
                            description,
                            date,
                            points
                        FROM Event
                        """
                    )
                    events = cur.fetchall()
                    return events
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException("Error getting events")

    def get_event(self, event_id: int):
        with pool.connection() as conn:
            with conn.cursor(row_factory=class_row(EventsOut)) as cur:
                cur.execute(
                    """
                    SELECT
                        event_id,
                        user_id,
                        title,
                        picture,
                        location,
                        description,
                        date,
                        points
                    FROM event
                    WHERE event_id = %s
                    """,
                    (event_id,),
                )
                event = cur.fetchone()
                return event
