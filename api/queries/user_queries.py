"""
Database Queries for Users
"""

import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from fastapi import HTTPException
from typing import Optional
from models.users import UserWithPw, UserDetailIn, UserDetailOut
from utils.exceptions import UserDatabaseException
from utils.authentication import hash_password


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class UserQueries:
    def get_by_username(self, username: str) -> Optional[UserWithPw]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(UserWithPw)) as cur:
                    cur.execute(
                        """
                            SELECT
                                *
                            FROM users
                            WHERE username = %s
                            """,
                        [username],
                    )
                    user = cur.fetchone()
                    if not user:
                        return None
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(f"Error getting user {username}")
        return user

    def get_by_id(self, user_id: int) -> Optional[UserWithPw]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(UserWithPw)) as cur:
                    cur.execute(
                        """
                            SELECT
                                *
                            FROM users
                            WHERE user_id = %s
                            """,
                        [user_id],
                    )
                    user = cur.fetchone()
                    if not user:
                        return None
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(
                f"Error getting user with id {user_id}"
            )
        return user

    # for sign-in-Up
    def create_user(
        self, username: str, hashed_password: str, email: Optional[str] = None
    ) -> UserWithPw:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(UserWithPw)) as cur:
                    params = [
                        username,
                        hashed_password,
                        email,
                    ]
                    cur.execute(
                        """
                        INSERT INTO users (
                            username, password, email
                        ) VALUES (
                            %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        params,
                    )
                    user = cur.fetchone()
                    if not user:
                        raise UserDatabaseException(
                            f"Could not create username {username}"
                        )
        except psycopg.Error:
            raise UserDatabaseException(
                f"Could not create username {username}"
            )
        return user

    def edit_user(self, username: str, users: UserDetailIn) -> UserDetailOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    params = [
                        username,
                        hash_password(users.password),
                        users.first_name,
                        users.last_name,
                        users.email,
                        users.date_birth,
                        users.phone_number,
                        users.address,
                        users.points,
                        users.role,
                        username,
                    ]
                    cur.execute(
                        """
                            UPDATE users
                            SET username = %s,
                                password = %s,
                                first_name = %s,
                                last_name = %s,
                                email = %s,
                                date_birth = %s,
                                phone_number = %s,
                                address = %s,
                                points = %s,
                                role = %s
                            WHERE username = %s
                            RETURNING user_id, username, password, first_name,
                            last_name, email, date_birth, phone_number,
                            address, created_at, points, role;
                            """,
                        params,
                    )
                    row = cur.fetchone()
                    if row is None:
                        raise HTTPException(
                            status_code=404,
                            detail=f"{username} not found",
                        )
                    return UserDetailOut(
                        user_id=row[0],
                        username=row[1],
                        password=row[2],
                        first_name=row[3],
                        last_name=row[4],
                        email=row[5],
                        date_birth=row[6],
                        phone_number=row[7],
                        address=row[8],
                        created_at=row[9],
                        points=row[10],
                        role=row[11],
                    )
        except Exception as e:
            print(f"An error occurred: {e}")
            raise HTTPException(
                status_code=500,
                detail="An error occurred while updating user details",
            )

    def delete_user(self, username: str):  # delete by username
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                            DELETE FROM users
                            WHERE username = %s;
                            """,
                        [username],
                    )
                    if cur.rowcount == 0:
                        raise UserDatabaseException(
                            f"User={username} not found "
                        )
        except psycopg.Error as e:
            raise UserDatabaseException(f"Delete failed. Error: {str(e)}")
