import os
from models.trips import TripsOut, TripsIn
from psycopg_pool import ConnectionPool
from fastapi import HTTPException
import psycopg
from utils.exceptions import UserDatabaseException
from typing import List

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class TripsQueries:

    def create_trip(self, trip: TripsIn) -> TripsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    params = [
                        trip.title,
                        trip.picture,
                        trip.location,
                        trip.description,
                        trip.date,
                        trip.trip_type,
                        trip.status,
                        trip.last_updated,
                    ]
                    cur.execute(
                        """
                        INSERT INTO trips (
                            title, picture, location, description, date,
                            trip_type, status, last_updated
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING trip_id, title, picture, location,
                        description, date, trip_type, status, last_updated;
                        """,
                        params,
                    )
                    row = cur.fetchone()
                    if row is None:
                        raise HTTPException(
                            status_code=500, detail="Failed to create trip"
                        )

                    return TripsOut(
                        trip_id=row[0],
                        title=row[1],
                        picture=row[2],
                        location=row[3],
                        description=row[4],
                        date=row[5],
                        trip_type=row[6],
                        status=row[7],
                        last_updated=row[8],
                    )
        except Exception as e:
            print(f"An error occurred: {e}")
            raise HTTPException(
                status_code=500,
                detail="An error occurred while creating the trip",
            )

    def get_trip_details(self, trip_id: int) -> TripsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT trips.trip_id, title, picture, location,
                        description, date, trip_type, status, last_updated
                        FROM trips
                        WHERE trips.trip_id = %s;
                        """,
                        (trip_id,),
                    )
                    trip = cur.fetchone()
                    if not trip:
                        raise HTTPException(
                            status_code=404,
                            detail=f"Trip with ID {trip_id} not found",
                        )
                    return TripsOut(
                        trip_id=trip[0],
                        title=trip[1],
                        picture=trip[2],
                        location=trip[3],
                        description=trip[4],
                        date=trip[5],
                        trip_type=trip[6],
                        status=trip[7],
                        last_updated=trip[8],
                    )
        except Exception as e:
            print(f"An error occurred: {e}")
            raise HTTPException(
                status_code=500,
                detail="An error occurred while retrieving trip details",
            )

    def get_all_trips(self) -> List[TripsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT
                            trip_id,
                            title,
                            picture,
                            location,
                            description,
                            date,
                            trip_type,
                            status,
                            last_updated
                        FROM trips
                        """
                    )
                    trips = cur.fetchall()
                    trip_objects = []
                    for trip in trips:
                        trip_obj = TripsOut(
                            trip_id=trip[0],
                            title=trip[1],
                            picture=trip[2],
                            location=trip[3],
                            description=trip[4],
                            date=str(trip[5]),
                            trip_type=trip[6],
                            status=trip[7],
                            last_updated=trip[8],
                        )
                        trip_objects.append(trip_obj)
                    return trip_objects
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException("Error getting trips")

    def edit_trip(self, trip_id: int, trip: TripsIn) -> TripsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    params = [
                        trip.title,
                        trip.picture,
                        trip.location,
                        trip.description,
                        trip.date,
                        trip.trip_type,
                        trip.status,
                        trip.last_updated,
                        trip_id,
                    ]
                    cur.execute(
                        """
                        UPDATE trips
                        SET title = %s,
                            picture = %s,
                            location = %s,
                            description = %s,
                            date = %s,
                            trip_type = %s,
                            status = %s,
                            last_updated = %s
                        WHERE trip_id = %s
                        RETURNING trip_id, title, picture, location,
                        description, date, trip_type, status, last_updated;
                        """,
                        params,
                    )
                    row = cur.fetchone()
                    if row is None:
                        raise HTTPException(
                            status_code=404,
                            detail=f"Trip with ID {trip_id} not found",
                        )
                    return TripsOut(
                        trip_id=row[0],
                        title=row[1],
                        picture=row[2],
                        location=row[3],
                        description=row[4],
                        date=row[5],
                        trip_type=row[6],
                        status=row[7],
                        last_updated=row[8],
                    )
        except Exception as e:
            print(f"An error occurred: {e}")
            raise HTTPException(
                status_code=500,
                detail="An error occurred while updating the trip",
            )

    def delete_trip(self, trip_id: int) -> None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM trips
                        WHERE trip_id = %s;
                        """,
                        (trip_id,),
                    )
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(
                f"Error deleting trip with ID {trip_id}"
            )
