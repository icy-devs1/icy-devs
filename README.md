# Module3 Project Gamma
ICY Trips Platform

Team
1. Team Member 1: Name: Angel Flores
-   Role: CSS NavBar & Footer signing/signup/StaffDashboard Frontend/Backend Functionality/ 1 Event backend endpoint, 2 Testing endpoints, 2 User endpoint
2. Team Member 2: Name: [Full Name]
-   Role: [Role in the Project]
3. Team Member 3: Name: Joshua Kahn
-   Role: CSS and Main Frontend Functionality/ 1 Event backend endpoint, 2 Testing endpoints, 1 User endpoint
4. Team Member 4: Name: LaTorya Walker
-   Role: Trips Backend/Trips Frontend/ 1 Event endpoint for the Backend, 2 Testing Endpoints, & Gitlabyaml + read.me

## Project Overview
ICY Trips is a platform designed to provide ICY participants, parents, and supporters with a comprehensive view of ICY trips and related activities. It enables students to propose new trips, allows admins to share upcoming events, and fosters community interaction. The platform includes the following core features and

## Core Features
    1. User Authentication:
    2. Users can create accounts, log in, and securely access their profiles.
    3. Profile management allows users to view and update personal information.
    4. Content Creation and Management:
    5. Users can upload photos, write trip highlight recaps, and create dream list trips to share travel experiences and aspirations.
    6. Content can be edited or deleted by the user.
    7. Event Sharing: Admins can create and manage events to inform students and parents about upcoming activities and opportunities.


## Getting started

You have a project repository, now what? The next section
lists all of the deliverables that are due at the end of the
week. Below is some guidance for getting started on the
tasks for this week.


## What you need
- Docker
- Git
- Node.js


## Install Extensions

**Clone the Repository**
   - Fork the repository from [GitLab Repository Link](https://gitlab.com/icy-devs1/icy-devs.git)
   - Clone the repository to your local machine:
     ```
     git clone https://gitlab.com/icy-devs1/icy-devs.git
     cd icy-devs

2. **Set Up Backend (FastAPI)**
   - Navigate to the `api` directory:
     ```
     cd api
     ```
   - Create a virtual environment and activate it:
     ```
     python -m venv .venv
     source .venv/bin/activate
     ```
   - Install dependencies:
     ```
     pip install -r requirements.txt

3. **Set Up Frontend (React)**
   - Navigate to the `ghi` directory:
     ```
     cd ../ghi
     ```
   - Install dependencies:
     ```
     npm install
     ```

## Deliverables

-   [ ] Wire-frame diagrams
-   [ ] API documentation
-   [ ] Project is deployed to Caprover (BE, DB) & GitLab-pages (FE)
-   [ ] GitLab issue board is setup and in use (or project management tool of choice)
-   [ ] Journals

4. **Start Docker Services**
   - Navigate back to the root directory and start Docker containers:
     ```
     docker-compose build
     docker-compose up

5. **Access the Application**
   - Once Docker containers are running, access the ICY Trips platform:
     - Backend (FastAPI): http://localhost:8000/docs
     - Frontend (React): http://localhost:5173

## Project layout

The layout of the project is just like all of the projects
you did with `docker-compose` in module #2. You will create
a directory in the root of the repository for each service
that you add to your project just like those previous
projects were setup.

### Directories

Several directories have been added to your project. The
directories `docs` and `journals` are places for you and
your team-mates to, respectively, put any documentation
about your project that you create and to put your
project-journal entries. See the _README.md_ file in each
directory for more info.

The other directories, `ghi` and `api`, are services, that
you can start building off of.

Inside of `ghi` is a minimal React app that has an "under construction" page.
This app is written using the [Vite](https://vitejs.dev/) bundler. The example
code is also using [jsdoc](https://jsdoc.app/) to provide type hints for
JavaScript. You are not required to use JSDoc yourself, and you will be removing
these examples and providing your own code for `App.jsx`

Inside of `api` is a minimal FastAPI application.
"Where are all the files?" you might ask? Well, the
`main.py` file is the whole thing, and go take look inside
of it... There's not even much in there..., hmm? That is
FastAPI, we'll learn more about it in the coming days. Can
you figure out what this little web-application does even
though you haven't learned about FastAPI yet?

Also in `api` is a directory for your migrations.
If you choose to use PostgreSQL, then you'll want to use
migrations to control your database. Unlike Django, where
migrations were automatically created for you, you'll write
yours by hand using DDL. Don't worry about not knowing what
DDL means; we have you covered. There's a sample migration
in there that creates two tables so you can see what they
look like.

The Dockerfile and Dockerfile.dev run your migrations
for you automatically.

### Other files

The following project files have been created as a minimal
starting point. Please follow the guidance for each one for
a most successful project.

-   `docker-compose.yaml`: there isn't much in here, just a
    **really** simple UI and FastAPI service. Add services
    (like a database) to this file as you did with previous
    projects in module #2.
-   `.gitlab-ci.yml`: This is your "ci/cd" file where you will
    configure automated unit tests, code quality checks, and
    the building and deployment of your production system.
    Currently, all it does is deploy an "under construction"
    page to your production UI on GitLab and a sample backend
    to CapRover. We will learn much more about this file.
-   `.gitignore`: This is a file that prevents unwanted files
    from getting added to your repository, files like
    `pyc` files, `__pycache__`, etc. We've set it up so that
    it has a good default configuration for Python projects.
-   `.env.sample`: This file is a template to copy when
    creating environment variables for your team. Create a
    copy called `.env` and put your own passwords in here
    without fear of it being committed to git (see `.env`
    listed in `.gitignore`). You can also put team related
    environment variables in here, things like api and signing
    keys that shouldn't be committed; these should be
    duplicated in your deployed environments.

### Installing python dependencies locally

In order for VSCode's built in code completion and intelligence to
work correctly, it needs the dependencies from the requirements.txt file
installed. We do this inside docker, but not in the workspace.

So we need to create a virtual environment and pip install the requirements.

From inside the `api` folder:

```bash
python -m venv .venv
```

Then activate the virtual environment

```bash
source .venv/bin/activate
```

And finally install the dependencies

```bash
pip install -r requirements.txt
```

Then make sure the venv is selected in VSCode by checking the lower right of the
VSCode status bar

### Setup GitLab repo/project

-   make sure this project is in a group. If it isn't, stop
    now and move it to a GitLab group
-   remove the fork relationship: In GitLab go to:

    Settings -> General -> Advanced -> Remove fork relationship

-   add these GitLab CI/CD variables:
    -   PUBLIC_URL : this is your gitlab pages URL
    -   VITE_APP_API_HOST: enter "blank" for now



## Project Impact
The ICY Trips platform aims to enrich educational experiences beyond the classroom by facilitating:
Transparency in trip information for participants and supporters.
Engagement through user-generated content and community interaction.
Tracking of eligibility and participation through a points system.
Accessibility and user experience enhancement with a potential mobile app conversion.

#### Your GitLab pages URL

You can't find this in GitLab until after you've done a deploy
but you can figure it out yourself from your GitLab project URL.

If this is your project URL

https://gitlab.com/GROUP_NAME/PROJECT_NAME

then your GitLab pages URL will be

https://GROUP_NAME.gitlab.io/PROJECT_NAME

## Frontend Breakdown

The ICY frontend utilizes React hooks and a host of functions to map, filter, and otherwise display information on pages directly from the database, pulling from the necessary endpoints. Though there are a few static elements, making current data available to the users was, and is, a very high priority. As far as design goes, our team took to using a blend of tailwind, bootstrap, and CSS elements for a simpler, but still streamlined, visual approach. Our CSS files have been organized in VSCode, and much of it was dedicated to proper page flow, as this was considered by the team to be an important piece in making the project more user friendly.

We adhered, for the most part, to our wireframe for design inspiration. It was constructed carefully at the start so that we had a solid idea of where we wanted to go with this project, serving as necessary guidance for many different elements.

![alt text](image-7.png)

## Backend Breakdown

Update User Endpoint
This allows for the manipulation of a user's information, which was supposed to be implemented as part of a stretch goal.
It can be found on the fastAPI docs page, and is currently functional.
Update User
Method: PUT
URL: /users/update
Description: Edit user information
Successful Response:
json -
![alt text](image-5.png)

Edit Event Endpoint
This is used to update event information, such as user_id, title, picture, location, description, date, and points. Event ID is automatically implemented and is not
affected by this endpoint. This can be found being used on the admin's events page, as only this role is allowed to edit events.
Edit Event
Method: PUT
URL: /events/{event_id}/
Description: Edit event information
Successful Response:
json -
![alt text](image-6.png)
## Trips Endpoint Documentation

## Frontend Endpoints- TRIPS
The frontend endpoints complement the backend by providing user interfaces and interactions for managing trips within the ICY Trips platform.



- **Create Trip**
  - Method: POST
  - URL: /trips
  - Description: Create a new trip.
  - Successful Response:

  ![alt text](image-5.png)

- **Edit Trip**
  - Method: PUT
  - URL: /trips/{trip_id}
  - Description: Edit details of a specific trip.
  - Successful Response:

![alt text](image-7.png)

  **Get All Trips**
  - Method: GET
  - URL: /trips
  - Description: Retrieve a list of all trips.
  - Successful Response:
    ```
![alt text](image-6.png)

    - **Delete Trip**
  - Method: DELETE
  - URL: /trips/{trip_id}
  - Description: Delete a specific trip.
  - Successful Response:
After you select delete
![alt text](image-8.png)


## Backend Endpoints- TRIPS

The backend endpoints are designed to manage trips within the ICY Trips platform. These endpoints allow interaction with trip data, enabling creation, retrieval, modification, and deletion of trips.

The following endpoints are available for managing trips on the backend. You can view the detailed documentation at localhost:8000/docs.
Trips Endpoints
Create Trip
Method: POST
URL: /trips
Description: Create a new trip.
Successful Response:
json:
![alt text](image.png)

Get Trip Details
Method: GET
URL: /trips/{trip_id}
Description: Retrieve details of a specific trip.
Successful Response:
json:
![alt text](image-1.png)

Edit Trip
Method: PUT
URL: /trips/{trip_id}
Description: Edit details of a specific trip.
Successful Response:
json

![alt text](image-2.png)

Get All Trips
Method: GET
URL: /trips
Description: Retrieve a list of all trips.
Successful Response:
json
![alt text](image-3.png)


Delete Trip
Method: DELETE
URL: /trips/{trip_id}
Description: Delete a specific trip.
Successful Response:
json

![alt text](image-4.png)

These endpoints allow interaction with trip data, facilitating creation, retrieval, modification, and deletion of trips within the ICY Trips platform. For detailed usage instructions and example responses, refer to the API documentation available at localhost:8000/docs.



## Frontend Endpoints- EVENTS



## Backend Endpoints- EVENTS





## Frontend Endpoints- USERS



## Backend Endpoints- USERS
